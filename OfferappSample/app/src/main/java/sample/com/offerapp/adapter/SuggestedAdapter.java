package sample.com.offerapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sample.com.offerapp.R;

/**
 * Created by luis on 13/12/2015.
 */
public class SuggestedAdapter extends ArrayAdapter<SuggestedAdapter.Suggested> {

    Context mContext;

    public SuggestedAdapter(Context context, int resource, List<Suggested> objects) {
        super(context, resource, objects);
        this.mContext=context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder holder = null;

        if(convertView == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.suggested_item, parent, false);

            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.suggested_name);
            holder.address = (TextView)convertView.findViewById(R.id.suggested_address);
            holder.imageProfile = (CircleImageView)convertView.findViewById(R.id.suggested_image_profile);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.populate(getItem(position), mContext);

        return convertView;
    }

   public static class Suggested {

        String urlImage;
        String name;
        String address;


        public  Suggested(JSONObject json){
            try {
                urlImage = json.getString("foto_perfil").replaceAll(" ","%20");
                name = json.getString("nombre");
                address = json.getString("direccion");
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    class ViewHolder{
        TextView name;
        TextView address;
        CircleImageView imageProfile;

        public void populate(Suggested suggested, Context context){
            name.setText(suggested.name);
            address.setText(suggested.address);
            Picasso.with(context).load(suggested.urlImage).into(imageProfile);
        }
    }
}



