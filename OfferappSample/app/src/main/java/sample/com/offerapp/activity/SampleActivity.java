package sample.com.offerapp.activity;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Toast;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import sample.com.offerapp.R;
import sample.com.offerapp.adapter.SuggestedAdapter;
import sample.com.offerapp.service.HttpService;
import sample.com.offerapp.adapter.PlacesAutoCompleteAdapter;

public class SampleActivity extends AppCompatActivity {

    public static SampleActivity self;
    private PlacesAutoCompleteAdapter mAdapter;
    private SuggestedAdapter mSuggestedAdapter;
    private List<SuggestedAdapter.Suggested> list= new ArrayList<>();
    String TAG ="TAG";
    HandlerThread mHandlerThread;
    Handler mThreadHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        self=this;
        AutoCompleteTextView autocompleteView = (AutoCompleteTextView) this.findViewById(R.id.autocomplete);
        mAdapter=new PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list_item);
        mSuggestedAdapter=new SuggestedAdapter(this,R.layout.suggested_item,list);
        final ListView listView =  (ListView)findViewById(R.id.listView);
        listView.setAdapter(mSuggestedAdapter);
        autocompleteView.setAdapter(mAdapter);
        autocompleteView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String data[] = ((String) parent.getItemAtPosition(position)).split("-");
                RequestParams params = new RequestParams();
                try {
                    params.put("city", data[0]);
                    params.put("state", data[1]);
                    params.put("country", data[2]);
                    params.put("email",URLEncoder.encode("bolivarbryan@gmail.com", "utf8"));
                    params.put("funcion", "list_suggested_stores");
                }catch (Exception e){
                    e.printStackTrace();
                }
                HttpService.post("funciones.php", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.e("RESPONSE",response.toString());
                        try {
                            if (response.getInt("success") == 1) {
                                JSONArray jsonArray = response.getJSONArray("sugeridas");
                                list.clear();
                                for (int i=0;i<jsonArray.length(); i++){
                                    list.add(new SuggestedAdapter.Suggested(jsonArray.getJSONObject(i)));
                                }
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }finally {
                            if(list.size()>0){
                                listView.setVisibility(View.VISIBLE);
                                findViewById(R.id.frMessage).setVisibility(View.GONE);
                            }
                            mSuggestedAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toast.makeText(self, responseString, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Toast.makeText(self, responseString + "SUCCESS", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

        if (mThreadHandler == null) {

            mHandlerThread = new HandlerThread(TAG,android.os.Process.THREAD_PRIORITY_BACKGROUND);
            mHandlerThread.start();
            mThreadHandler = new Handler(mHandlerThread.getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == 1) {
                        ArrayList<String> results = mAdapter.resultList;

                        if (results != null && results.size() > 0) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyDataSetChanged();
                                }
                            });

                        }
                        else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyDataSetInvalidated();
                                }
                            });

                        }
                    }
                }
            };
        }
        autocompleteView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String value = s.toString();
                mThreadHandler.removeCallbacksAndMessages(null);
                mThreadHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.resultList = mAdapter.mPlaceAPI.autocomplete(value);
                        if (mAdapter.resultList.size() > 0)
                            mAdapter.resultList.add("footer");
                        mThreadHandler.sendEmptyMessage(1);
                    }
                }, 500);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //doAfterTextChanged();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


        if (mThreadHandler != null) {
            mThreadHandler.removeCallbacksAndMessages(null);
            mHandlerThread.quit();
        }
    }
}
